# README
A prestigious international non-profit organization, StopDeath.org, has
sought out your expertise as a highly-skilled front-end developer. They've
commissioned a design mockup from a well-known designer, but their resources
have fallen short on implementation. Your task is to rescue StopDeath.org's
project and see the implementation to completion.

## Minimum Requirements
  1. Recreate the provided mockup as a production quality prototype.
  You should:
    - style for all commonly used HTML elements
    - use the most semantically correct markup.
  1. Explain, in detail, your dev and decision making process in the README.md.
  1. You will only receive the desktop version of the design mock-up. It is up to you to style for additional viewports. Make sure you explain your responsive decisions.
  1. WCAG level A compliance.

### Bonus points
1. Consideration and documentation of states and interactions.
1. Prototype or working demo (double bonus: in a Drupal or Wordpress site).
1. A style guide to outline your design framework.
1. Use a CSS/JS compiler, webpack, gulp, etc. in your build pipeline, with documentation.
1. Design fluorishes, rich elements, animations, etc.
1. Deliver a Drupal or WordPress theme or sub-theme.
1. Extra attention paid to creating performant assets, with mobile-friendly load times.
1. WCAG level AA compliance.
1. Additional code inclusions geared towards SEO.

## Additional information
* If you run out of time, or are unable to fulfill some part of the assignment,
explain the solution you _would_ build.
* Show your work! We'll consider your PR for partial credit even if you don't
fulfill the requirements.
* Relying on existing libraries and 3rd-party code is encouraged so
long as you _credit the original source_. *Don't reinvent the wheel!*
* We expect a theme based on the generic mock-up, but you have some
degree of creative freedom to adjust as necessary, keeping Stop Death's brand
in mind. If you decide to go this route, consider tweaking the final site in
ways the client would appreciate, or that you feel would amplify the client's
message. Be prepared to defend your decisions.
* This repository, https://bitbucket.org/messageagency/frontend_assessment,
is more or less irrelevant to your PR. We know that's not a standard use case
for pull requests, but it's the easiest way for us to collect and analyze responses.
* You may continue committing up until the submission deadline. No commits made
after submission deadline will be considered.
* Similarly, feel free to provide details about known bugs, areas for
improvement, or any other possible enhancements.
* Comment your work AND provide documentation please. We want to see how you
tackle a problem, and what your thinking is when solving it, and what resources
you used to get to your solution.
* In addition to code comments, we'll look at your commit logs to get an idea
of your process.
* Please keep the repository lean, especially if you're relying on 3rd party
libraries. We don't need an entire Wordpress install, for example.

## Instructions
1. Create a fork for your work (either a public fork, or grant access to reviewers)
1. Fulfill the implementation requirements
1. Create a pull request with your submission

## Questions?
* Please contact the repository owner with any questions or concerns.
